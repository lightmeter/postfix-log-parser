# Postfix Log Parser Go Library

[![pipeline status](https://gitlab.com/lightmeter/postfix-log-parser/badges/master/pipeline.svg)](https://gitlab.com/lightmeter/postfix-log-parser/-/commits/master)
[![coverage report](https://gitlab.com/lightmeter/postfix-log-parser/badges/master/coverage.svg)](https://gitlab.com/lightmeter/postfix-log-parser/-/commits/master)
[![report_card](https://goreportcard.com/badge/gitlab.com/lightmeter/postfix-log-parser)](https://goreportcard.com/report/gitlab.com/lightmeter/postfix-log-parser)
[![scale_index](https://sonarcloud.io/api/project_badges/measure?project=lightmeter_postfix-log-parser&metric=sqale_index)](https://sonarcloud.io/dashboard?id=lightmeter_postfix-log-parser)
[![bugs](https://sonarcloud.io/api/project_badges/measure?project=lightmeter_postfix-log-parser&metric=bugs)](https://sonarcloud.io/dashboard?id=lightmeter_postfix-log-parser)
[![code_smells](https://sonarcloud.io/api/project_badges/measure?project=lightmeter_postfix-log-parser&metric=code_smells)](https://sonarcloud.io/dashboard?id=lightmeter_postfix-log-parser)
[![coverage](https://sonarcloud.io/api/project_badges/measure?project=lightmeter_postfix-log-parser&metric=coverage)](https://sonarcloud.io/dashboard?id=lightmeter_postfix-log-parser)
[![duplicated_lines_density](https://sonarcloud.io/api/project_badges/measure?project=lightmeter_postfix-log-parser&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=lightmeter_postfix-log-parser)
[![ncloc](https://sonarcloud.io/api/project_badges/measure?project=lightmeter_postfix-log-parser&metric=ncloc)](https://sonarcloud.io/dashboard?id=lightmeter_postfix-log-parser)
[![sqale_rating](https://sonarcloud.io/api/project_badges/measure?project=lightmeter_postfix-log-parser&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=lightmeter_postfix-log-parser)
[![alert_status](https://sonarcloud.io/api/project_badges/measure?project=lightmeter_postfix-log-parser&metric=alert_status)](https://sonarcloud.io/dashboard?id=lightmeter_postfix-log-parser)
[![reliability_rating](https://sonarcloud.io/api/project_badges/measure?project=lightmeter_postfix-log-parser&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=lightmeter_postfix-log-parser)
[![security_rating](https://sonarcloud.io/api/project_badges/measure?project=lightmeter_postfix-log-parser&metric=security_rating)](https://sonarcloud.io/dashboard?id=lightmeter_postfix-log-parser)
[![sqale_index](https://sonarcloud.io/api/project_badges/measure?project=lightmeter_postfix-log-parser&metric=sqale_index)](https://sonarcloud.io/dashboard?id=lightmeter_postfix-log-parser)
[![vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=lightmeter_postfix-log-parser&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=lightmeter_postfix-log-parser)

This is an eternal work in progress Postfix Log Parser used by Lightmeter Control Center.

It's very imcomplete as it for now parses only the log types that we use on Lightmeter.

If you have interest on improving it, please send us a PR and check the Lightmeter Contributor agreement in the file CLA.

For licencing information, please check the file LICENSE.

Lightmeter Team
https://lightmeter.io
